{-# LANGUAGE OverloadedStrings #-}

module XmlRpc.Client2
    (
     remote,
     call,
     Remote
    ) where

import Network.XmlRpc.Internals

import Control.Monad.IO.Class (liftIO)

import Network.HTTP.Conduit
import Data.Conduit
import qualified Data.ByteString.Lazy as L 
import qualified Data.ByteString.Lazy.Char8 as LC 
 
-- | Gets the return value from a method response.
--   Throws an exception if the response was a fault.
handleResponse :: Monad m => MethodResponse -> m Value
handleResponse (Return v) = return v
handleResponse (Fault code str) = fail ("Error " ++ show code ++ ": " ++ str)

-- | Sends a method call to a server and returns the response.
--   Throws an exception if the response was an error.
doCall :: String -> MethodCall -> Err IO MethodResponse
doCall url mc = 
    do 
    let req = renderCall mc
    resp <- ioErrorToErr $ post url req
    parseResponse resp

-- | Low-level method calling function. Use this function if
--   you need to do custom conversions between XML-RPC types and 
--   Haskell types.
--   Throws an exception if the response was a fault.
call :: String -- ^ URL for the XML-RPC server.
     -> String -- ^ Method name.
     -> [Value] -- ^ The arguments.
     -> Err IO Value -- ^ The result
call url method2 args = doCall url (MethodCall method2 args) >>= handleResponse


-- | Call a remote method.
remote :: Remote a => 
	  String -- ^ Server URL. May contain username and password on
	         --   the format username:password\@ before the hostname.
       -> String -- ^ Remote method name.
       -> a      -- ^ Any function 
		 -- @(XmlRpcType t1, ..., XmlRpcType tn, XmlRpcType r) => 
                 -- t1 -> ... -> tn -> IO r@
remote u m = remote_ (\e -> "Error calling " ++ m ++ ": " ++ e) (call u m)

class Remote a where
    remote_ :: (String -> String)        -- ^ Will be applied to all error
					 --   messages.
	    -> ([Value] -> Err IO Value) 
	    -> a

instance XmlRpcType a => Remote (IO a) where
    remote_ h f = handleError (fail . h) $ f [] >>= fromValue

instance (XmlRpcType a, Remote b) => Remote (a -> b) where
    remote_ h f x = remote_ h (\xs -> f (toValue x:xs))


-- | Post some content to a uri, return the content of the response
--   or an error.
-- FIXME: should we really use fail?
post :: String -> L.ByteString -> IO String
post uri content = runResourceT $ do
    m <- liftIO $ newManager def
    request <- liftIO $ parseUrl uri
    let request2 = appendBody request content
    fmap (LC.unpack . responseBody) $ httpLbs request2 m
    where
        appendBody req body = req
            { requestBody = RequestBodyLBS body
            , method = "POST"
            , requestHeaders =
                (ct, "text/xml") : filter (\(x, _) -> x /= ct) (requestHeaders req)
            }
          where
            ct = "Content-Type"

