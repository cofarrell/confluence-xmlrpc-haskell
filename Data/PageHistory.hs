{-# OPTIONS_GHC -fth #-}

module Data.PageHistory where

import System.Time (CalendarTime(..))
import Prelude hiding (id)
import Network.XmlRpc.THDeriveXmlRpcType 

data PageHistory = PageHistory {
                   id       :: String
                 , version  :: String
                 , modifier :: String
                 , modified :: CalendarTime
                 } deriving Show

$(asXmlRpcStruct ''PageHistory)
