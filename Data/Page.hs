{-# OPTIONS_GHC -fth #-}

module Data.Page where

import System.Time (CalendarTime(..))
import Prelude hiding (id)
import Network.XmlRpc.THDeriveXmlRpcType 

data Page = Page { id            :: String
                 , parentId      :: String
                 , title         :: String
                 , url           :: String
                 , version       :: String
                 , content       :: String
                 , created       :: CalendarTime
                 , creator       :: String
                 , modified      :: CalendarTime
                 , modifier      :: String
                 , homePage      :: String
--               , locks         :: String
                 , contentStatus :: String
                 , current       :: String
                 } deriving Show

$(asXmlRpcStruct ''Page)
