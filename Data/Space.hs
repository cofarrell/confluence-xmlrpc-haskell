{-# OPTIONS_GHC -fth #-}

module Data.Space where

import Network.XmlRpc.THDeriveXmlRpcType

data Space = Space { url:: String, name:: String, key:: String } deriving Show

$(asXmlRpcStruct ''Space)
