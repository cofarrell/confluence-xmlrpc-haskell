module Main where

import XmlRpc.Client2
import qualified Data.Page as P
import qualified Data.PageHistory as PH
import qualified Data.Space as S

-- https://developer.atlassian.com/display/CONFDEV/Confluence+XML-RPC+and+SOAP+APIs
server :: String
server = "http://host:port/" ++ "/rpc/xmlrpc"

invokeV2 name = remote server ("confluence2." ++ name)

login :: String -> String -> IO String
login username password = invokeV2 "login" username password

logout :: String -> IO Bool
logout token = invokeV2 "logout" token

getSpaces :: String -> IO [S.Space]
getSpaces token = invokeV2 "getSpaces" token

getPage :: String -> String -> IO P.Page
getPage token pageId = invokeV2 "getPage" token pageId

getPageST :: String -> String -> String -> IO P.Page
getPageST token space title = invokeV2 "getPage" token space title

getPageHistory :: String -> String -> IO [PH.PageHistory]
getPageHistory token space = invokeV2 "getPageHistory" token space

main = do
       putStr "Username: "
       username <- getLine
       putStr "Password: "
       password <- getLine
       token <- login username password
       putStrLn "Getting page"
       -- page2 <- getPage token "123456"
       -- putStrLn . show $ page2
       page <- getPageST token "SPC" "Some title"
       putStrLn . show $ page
       ph <- getPageHistory token (P.id page)
       putStrLn . unlines . map show . take 5 $ ph
       putStrLn "Getting spaces"
       spaces <- getSpaces token
       putStrLn . unlines . map show . take 5 $ spaces
       logout token
